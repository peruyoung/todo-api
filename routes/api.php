<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('users','Api\FrontController@index');
Route::get('listado','Api\FrontController@listado');
Route::post('registro','Api\FrontController@registro');
Route::post('actualizar/{id}','Api\FrontController@actualizar');
Route::post('actualizarimg/{id}','Api\FrontController@actualizarimg');
