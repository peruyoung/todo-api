<?php
 
namespace App\Http\Controllers\Api;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Listado;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;

class FrontController extends Controller
{
    public function index()
    {
        return User::all();
    }
    public function listado()
    {
        return Listado::orderBy('id', 'desc')->get();
    }
    public function registro(Request $request)
    {
        

        $Listados = new Listado();
        $Listados->descripcion = $request->input('descripcion');
        if (Input::hasFile('archivo'))
        {
            $path = $request->file('archivo')->store('upload/img');
            $save = Storage::disk('uploads')->put('img', $request->file('archivo'));
            $Listados->archivo = env('APP_URL').'/'.$path;
        }
        $Listados->estado = 0;
        $Listados->save();
        //return env('APP_URL').'/'.$path;
    }
    public function actualizar(Request $request, $id)
    {
        $Listados = Listado::find($id);
        $Listados->descripcion = $request->input('descripcion');
        $Listados->estado = $request->input('estado');;
        $Listados->save();
        return 'ok';
    }
    public function actualizarimg(Request $request, $id)
    {
        
        if (Input::hasFile('archivo'))
        {
            $path = $request->file('archivo')->store('upload/img');
            $save = Storage::disk('uploads')->put('img', $request->file('archivo'));
            $Listados = Listado::find($id);
            $Listados->archivo = env('APP_URL').'/'.$path;
            $Listados->save();
        }
        return 'ok';
    }
}