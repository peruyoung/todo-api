<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listado extends Model
{
    protected $table = "listas";
    public $primaryKey = "id";
    protected $fillable = [
        'descripcion',
        'estado',
        'archivo'
    ];
    public $timestamps = false;
}
